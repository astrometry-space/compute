import os, pathlib, logging, subprocess, hashlib, boto3, json, uuid
from astrometry.util.image2pnm import image2pnm
from glob import glob
from datetime import datetime
from botocore.client import Config, ClientError
import common, utils, agent, arguments
from dotenv import load_dotenv
load_dotenv()

logger  = common.getLogger(__name__)

# attempt to load magic:
try:
    import magic
except:
    magic = False
    logger.warning("Could not load magic, will not set MIME types when performing S3 uploads")

class Solve:

    def __init__(self):

        self.status = None
        self.status_log = []
    
    def pre_solve(self):
        """
        stuff to do before solving
        """
        self.ASTROMETRY_DOWNLOAD_FILE = os.getenv('ASTROMETRY_INPUT_FILE', False)
        # shut it down if no input image was provided:
        if not self.ASTROMETRY_DOWNLOAD_FILE:
            logger.error("No input file provided!")
            raise(Exception("No input file provided!"))
        # determine if it's S3 or an external download:
        if self.ASTROMETRY_DOWNLOAD_FILE.lower().startswith('http://') or self.ASTROMETRY_DOWNLOAD_FILE.lower().startswith('https://'):
            self.DOWNLOAD_FROM_S3 = False
        else:
           self. DOWNLOAD_FROM_S3 = True
        # the job id can be specified, else generated from UUIDv4:
        self.ASTROMETRY_JOB_ID = os.getenv('ASTROMETRY_JOB_ID',str(uuid.uuid4()))
        print(self.ASTROMETRY_JOB_ID)
        # all the work will occur in /tmp in a directory named using the job id:
        self.WORKDIR = pathlib.Path('/tmp').joinpath(self.ASTROMETRY_JOB_ID)
        # the job prefix the S3 directory that holds all the job results:
        self.ASTROMETRY_JOB_PREFIX = os.getenv('ASTROMETRY_JOB_PREFIX','jobs')
        # now make the workspace (if necessary):
        if self.WORKDIR.is_dir():
            logger.debug("Working directory exists: {}".format(self.WORKDIR))
        else:
            logger.debug("Creating working directory: {}".format(self.WORKDIR))
            os.makedirs(self.WORKDIR)
        # make an S3 client:
        self.ASTROMETRY_S3_ENDPOINT_URL = os.getenv('ASTROMETRY_S3_ENDPOINT_URL','http://s3.amazonaws.com')
        self.ASTROMETRY_BUCKET = os.getenv('ASTROMETRY_BUCKET','astrometry')
        self.AWS_DEFAULT_REGION = os.getenv('AWS_DEFAULT_REGION','us-east-1')
        self.s3 = boto3.resource('s3',endpoint_url=self.ASTROMETRY_S3_ENDPOINT_URL,region_name=self.AWS_DEFAULT_REGION,config=Config(signature_version='s3v4'))
        # check if bucket exists:
        try:
            self.s3.meta.client.head_bucket(Bucket=self.ASTROMETRY_BUCKET)
            self.bucket = self.s3.Bucket(self.ASTROMETRY_BUCKET)
        except ClientError:
            logger.info('Creating S3 bucket')
            config = {'LocationConstraint': self.AWS_DEFAULT_REGION}
            self.bucket = self.s3.create_bucket(Bucket=self.ASTROMETRY_BUCKET)#CreateBucketConfiguration=config)  
        self.agent = agent.Agent(self.ASTROMETRY_JOB_ID)
        self.agent.setStatus('bootstrapped')

    def solve(self):
        """
        do it to it
        """
        self.agent.stage = 'solve'
        self.agent.setStatus('starting')
        # the input file is within the working directory, named `field` but with the same name as the file on S3:
        ext = pathlib.Path(self.ASTROMETRY_DOWNLOAD_FILE).suffix

        # TODO: does forcing jpeg really make sense?
        # if it didn't have an extension, assume jpg:
        if ext == '':
            ext = '.jpg'

        self.ASTROMETRY_INPUT_FILE = self.WORKDIR.joinpath('field' + ext).as_posix()
        # download the source image:
        self.agent.setStatus('download pending')
        if self.DOWNLOAD_FROM_S3:
            logger.info("Downloading input file from S3: {} to {}".format(self.ASTROMETRY_DOWNLOAD_FILE, self.ASTROMETRY_INPUT_FILE))
            self.bucket.download_file(self.ASTROMETRY_DOWNLOAD_FILE,self.ASTROMETRY_INPUT_FILE)
        else:
            logger.info("Downloading input file over http: {}".format(self.ASTROMETRY_DOWNLOAD_FILE))
            subprocess.Popen(['wget','-O',self.ASTROMETRY_INPUT_FILE,self.ASTROMETRY_DOWNLOAD_FILE],cwd=self.WORKDIR.as_posix()).wait()
        self.agent.setStatus('download complete')

        # first, put the file back to s3:
        if magic:
            mimetype = magic.from_file(self.ASTROMETRY_INPUT_FILE, mime=True)
        else:
            mimetype = 'application/octet-stream'
        UPLOAD_PATH = pathlib.Path(self.ASTROMETRY_JOB_PREFIX).joinpath(self.ASTROMETRY_JOB_ID,'original').as_posix()
        logger.debug("Uploading original file to S3 key: {}".format(UPLOAD_PATH))
        self.bucket.upload_file(self.ASTROMETRY_INPUT_FILE,UPLOAD_PATH,ExtraArgs={'ContentType': mimetype})

        # Convert the original file to a jpeg for the UI:
        # https://github.com/dstndstn/astrometry.net/blob/08989cb87fa6f05a507fd77df2b08d74bad7183a/net/models.py#L476
        try:
            pnmfile = self.WORKDIR.joinpath('original.ppm').as_posix()
            pnmjpgfile = self.WORKDIR.joinpath('original.jpg').as_posix()
            image2pnm(self.ASTROMETRY_INPUT_FILE, pnmfile, force_ppm=True)
            cmd = ['pnmtojpeg']
            with open(pnmfile,'rb') as infile:
                with open(pnmjpgfile,'wb') as outfile:
                    proc = subprocess.Popen(cmd,stdin=infile,stdout=outfile)
                    proc.wait()
            UPLOAD_PATH = pathlib.Path(self.ASTROMETRY_JOB_PREFIX).joinpath(self.ASTROMETRY_JOB_ID,'original.jpg').as_posix()
            logger.debug("Uploading JPEG of original file to S3 key: {}".format(UPLOAD_PATH))
            self.bucket.upload_file(pnmjpgfile,UPLOAD_PATH,ExtraArgs={'ContentType': mimetype})
        except Exception as e:
            logger.warning('Error while converting input field to ppm')
            logger.warning(e)

        # run the solver:
        self.agent.setStatus('running')
        logger.info('starting solver...')
        solve_log_file = self.WORKDIR.joinpath('solve.log')
        with open(solve_log_file,'a') as solve_log:
            import time
            time.sleep(10)
            try:
                # attempt to load solve arguments from the environment as a JSON string:
                args = json.loads(os.getenv('ASTROMETRY_SOLVE_ARGS'))
            except:
                args = {
                    'z': 2,
                }
            options = arguments.Arguments().buildArgs(args)
            cmd = ['solve-field'] + options + [self.ASTROMETRY_INPUT_FILE]
            logger.info("solve-field command: `{}`".format(' '.join(cmd)))

            # display output, and log to file: https://stackoverflow.com/questions/15535240/python-popen-write-to-stdout-and-log-file-simultaneously
            proc = subprocess.Popen(cmd, cwd=self.WORKDIR.as_posix(), stdout=subprocess.PIPE, stderr=subprocess.STDOUT, universal_newlines=True)
            for line in proc.stdout:
                logger.info(line)
                solve_log.write(line)
            proc.wait()

        logger.info('solve-field exited complete.')
        self.agent.setStatus('complete')

    def post_solve(self):
        """
        stuff to do after solving
        """
        # this will hold the query params passed to the delete hook:
        P = {}
        self.agent.stage = 'post solve'
        self.agent.setStatus('processing')
        # if it solved:
        solve_bit = self.WORKDIR.joinpath('field.solved')
        if solve_bit.exists():
            P['solved'] = True
            if solve_bit.stat().st_size == 1:
                logger.info("field solved successfully!")
                calibration = utils.wcs2json(self.WORKDIR,'field.wcs')
                stars = utils.getObjectsInField(self.WORKDIR,'field.wcs')
        # TODO: handle failed calibrations:
        else:
            P['solved'] = False
            logger.info("calibration failed :(")
        
        # TODO: generate image thumbnails for the UI


        # upload all the files in the work directory:
        self.agent.setStatus('uploading artifacts')
        files = glob(self.WORKDIR.as_posix()+'/*')
        for f in files:
            FILE = pathlib.Path(f)
            if magic:
                mimetype = magic.from_file(FILE.as_posix(), mime=True)
            else:
                mimetype = 'application/octet-stream'
            NAME = FILE.name
            UPLOAD_PATH = pathlib.Path(self.ASTROMETRY_JOB_PREFIX).joinpath(self.ASTROMETRY_JOB_ID,NAME).as_posix()
            logger.debug("Uploading {} to S3 path {}".format(FILE,UPLOAD_PATH))
            self.bucket.upload_file(FILE.as_posix(),UPLOAD_PATH,ExtraArgs={'ContentType': mimetype})
        logging.info('Done uploading job files to S3.')
        
        # kill the task using the agent:
        self.agent.delete(P)

    def all(self):
        """
        complete solve
        """
        self.pre_solve()
        self.solve()
        self.post_solve()

if __name__ == "__main__":
    Solve().all()