import json, subprocess
import common

logger = common.getLogger(__name__)

def removeLeadingDashes(s):
    while s.startswith('-'):
        s = s[1:]
    return s

class Arguments:

    def __init__(self):
        self.loadMap()

    def loadMap(self):
        """
        get available options from solve-field help
        """
        raw = subprocess.check_output(["solve-field","-h"]).decode('utf-8')
        r = [ q.split(':')[0] for q in raw.split('Options include:\n')[1].split('\n') if q.strip().startswith('-') and ':' in q ]
        M = {}
        for q in r:
            # first split by carret:
            v = q.strip().replace(' /','').split('<')
            # determine if it accepts an input:
            if len(v) > 1:
                accepts = ''.join(v[1:]).replace('<','').replace('>','').lower()
            else:
                accepts = False
            for x in v[0].split(' '):
                if x.startswith('--'):
                    M[removeLeadingDashes(x)] = {
                        'accepts': accepts,
                        'arg': x
                    }
                elif x.startswith('-'):
                    M[removeLeadingDashes(x)] = {
                        'accepts': accepts,
                        'arg': x
                    }
        self.map = M

    def buildArgs(self,args):
        """
        given a dictionary of arguments, commpare them to the map and build an array of subprocess args
        """
        cmd = []
        for a in args.keys():
            if a in self.map.keys():
                if self.map[a]['accepts']:
                    if args[a]:
                        cmd.append(self.map[a]['arg'])
                        cmd.append(str(args[a]))
                    else:
                        logger.warning('no input provided for option "{}"'.format(a))
                else:
                    if args[a]:
                        logger.warning('ignoring provided input for option "{}"'.format(a))
                    cmd.append(self.map[a]['arg'])

            else:
                logger.warning('option "{}" not in input map!'.format(a))
        return cmd

if __name__ == "__main__":
    A = Arguments()
    # print(json.dumps(A.map,indent=4))
    args = {
        'z': 7,
        'fits-image': 'bar',
        'foo': None,
        'D': None,
        'ra': 5
    }
    print(A.buildArgs(args))