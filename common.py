def getLogger(logger_name):
    """
    get a logger that's consistently formatted
    """
    import logging, os
    if os.getenv('HUMAN_LOGS',0) == '1':
        LOG_FORMAT = '%(asctime)s,%(msecs)03d %(levelname)-5s %(filename)-12s %(funcName)-13s %(message)s'
        DATE_FORMAT = "%H:%M:%S"
    else:
        LOG_FORMAT = '{"timestamp":"%(asctime)-15s", "level":"%(levelname)s", "filename":"%(filename)s" "functionname":"%(funcName)s" "message":"%(message)s"}'
        DATE_FORMAT = "%Y-%m-%dZ%H:%M:%S"
    logging.basicConfig(format=LOG_FORMAT,datefmt=DATE_FORMAT)
    logger = logging.getLogger(logger_name)
    LOG_LEVEL = os.getenv('LOG_LEVEL','INFO')
    logger.setLevel(LOG_LEVEL)
    return logger