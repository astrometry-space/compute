# k8s Demonstration
These demonstrate how the solver might be run as a Kubernetes job pod.  The current configuration runs locally, using local indices in `/Volumes/astrometry/index` and uploads to S3 and the deployed API.

Use `envsubst` to interoplate AWS credentials (or whatever credentials you require) and pipe that into `kubectl`:
```bash
envsubst < job.yml | kubectl -f -
pod_name=$(kubectl describe job/compute | grep "Created pod:" | cut -d':' -f2 | xargs)
kubectl logs -f $pod_name
kubectl delete job/compute
```