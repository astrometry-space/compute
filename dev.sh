# source this to help with development ontop of the actual base image:
docker run \
    -it \
    --rm \
    -v $(pwd):/app \
    --workdir /app \
    -v astrometry_index:/usr/share/astrometry \
    --network astrometry \
    --env-file $(pwd)/.env \
    registry.gitlab.com/astrometry-space/base-images/debian:569931798_fc7e9fc8 bash