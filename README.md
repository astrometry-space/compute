# Astrometry Compute Container
Cloud-friendly astrometry.

This is intended to be instantiated via an orchestration API (Docker, Swarm, Kubernetes, ECS, etc.) by a server.

This container will:
  * Download the image specified in the environment from S3
  * Solve the image, adding any configurations specified in the environment
  * Upload all job files back to S3
  

|   Environment Variable    | Required |          Default          | Description                                                                 |
| :-----------------------: | :------: | :-----------------------: | :-------------------------------------------------------------------------- |
|         LOG_LEVEL         |    No    |          `INFO`           | Python log level defaults to `INFO`                                         |
|        HUMAN_LOGS         |    No    |            `0`            | When set to `1` disables JSON logging for easier debugging                  |
|  ASTROMETRY_S3_ENDPOINT   |   Yes    |    `http://minio:9000`    | Object storage endpoint                                                     |
| ASTROMETRY_AGENT_ENDPOINT |   Yes    | `http://agent:3000/agent` | Adress (with protocol) of the agent API                                     |
|     ASTROMETRY_TOKEN      |    No    |          `NONE`           | Secret token used with agent API, unique to each job                        |
|     ASTROMETRY_BUCKET     |   Yes    |       `astrometry`        | Bucket that holds all data                                                  |
|   ASTROMETRY_JOB_PREFIX   |   Yes    |          `jobs`           | S3 prefix containing all job results                                        |
|   ASTROMETRY_INPUT_FILE   |   Yes    |          `NONE`           | Full path to input file within the `ASTROMETRY_BUCKET`                      |
|   ASTROMETRY_SOLVE_ARGS   |    No    |    `{"hello":"world"}`    | JSON string providing custom flags to the `solve-field` command             |
|     ASTROMETRY_JOB_ID     |    No    |          `NONE`           | Job ID used to identify the run.  If unset, will be generated automatically |
|     AWS_ACCESS_KEY_ID     |   Yes    |          `NONE`           | Standard AWS credential for accessing S3                                    |
|   AWS_SECRET_ACCESS_KEY   |   Yes    |          `NONE`           | Standard AWS credential for accessing S3                                    |
|    AWS_DEFAULT_REGION     |   Yes    |        `us-east-1`        | AWS regions (sometimes required, so I threw it in)                          |

# Development

Configure minio locally [as shown here](https://gitlab.com/astrometry-space/minio)

On OS X Catalina install the astrometry.net code using `pip` and `homebrew`:
```bash
# not sure this is strictly necessary:
pip3 install astropy
brew update && brew install astrometry-new && \
solve-field --help
```
On OS X, my astrometry engine configuration file was located at `/usr/local/etc/astrometry.cfg`.  In a text editor look for the `add_path` argument.  Then add a new entry called `/Volumes/astrometry/index`.

**NOTE:** I chose the path under `/Volumes` because it is shared with the Docker VM by default in Docker for Mac.  If you use another location, ensure that part of your filesystem is shared with Docker in Docker for Mac preferences|

```bash
...
# In which directories should we search for indices?
add_path /usr/local/Cellar/astrometry-net/0.80_1/data
# I added my own, non-default location:
add_path /Volumes/astrometry/index
...
```
Now, make that directory:
```bash
sudo mkdir -p /Volumes/astrometry/index
```
Then download some index files (it should minimially work with 4100):
```bash
cd /Volumes/astrometry/index
sudo wget -r -np -nd -A fits "http://data.astrometry.net/4100/"
```
Now, you should be able to download a starfield (not super narrow) and solve it.
```bash
cd ~/Desktop
wget http://nova.astrometry.net/image/8356739 -O demo.jpg
solve-field -z 2 ./demo.jpg
```
### Optional requirements
`solve.py` will attempt to determine the mime type of all S3 uploads.  To do this is uses `python-magic` which depends upon the `libmagic` binaries.  These already exist in the Linux base image, but would need to be installed when running OS X:
```bash
brew update & brew install libmagic
```

## OK, and now in Docker
I use the following to create the `astrometry_index` docker volume that the compute container mounts.  In this way I can use the index files both on the host solver (as above) and inside the container.  If I download more index files to that directory, they will be global usable.
```bash
ASTROMETRY_INDEX_FOLDER=/Volumes/astrometry/index
docker volume create --driver local -o o=bind -o type=none -o device=$ASTROMETRY_INDEX_FOLDER astrometry_index
```