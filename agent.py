import requests, os
import urllib.parse
from datetime import datetime

from dotenv import load_dotenv
load_dotenv()

class Agent():

    def __init__(self,jobid=None):
        self.stage = 'init'
        self.agent_endpoint = os.getenv('ASTROMETRY_AGENT_ENDPOINT','http://agent:3000/agent')
        # a lazy way to handle trailing slashes on our endpoint:
        while self.agent_endpoint[-1] == '/':
            self.agent_endpoint = self.agent_endpoint[:-1]
        if jobid:
            self.jobid = jobid
        else:
            self.jobid = os.getenv('ASTROMETRY_JOB_ID')

    def delete(self,D=None):
        """
        tell the agent to kill this job
        D: body to send with the request
        """
        self.stage = 'death'
        self.setStatus('hitting delete hook')
        url = '/'.join([self.agent_endpoint,'delete',self.jobid])
        # if available, get secret key from environment:
        if "ASTROMETRY_TOKEN" in os.environ:
            D['token'] = os.getenv('ASTROMETRY_TOKEN')
        return requests.post(url,json=D)

    def setStatus(self,message):
        """
        register the current status with the agent
        """
        url = '/'.join([self.agent_endpoint,'status',self.jobid,self.stage,message])
        return requests.post(url)
        