FROM registry.gitlab.com/astrometry-space/base-images/debian:569931798_fc7e9fc8

WORKDIR /tmp

RUN curl https://bootstrap.pypa.io/get-pip.py -o get-pip.py && python3 get-pip.py && rm -f get-pip.py

WORKDIR /app

ADD . .

RUN pip3 install -r requirements.txt

ENTRYPOINT [ "python3", "solve.py" ]