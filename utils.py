import json, subprocess
from pathlib import Path

def wcs2json(WORKDIR,wcs_file='field.wcs'):
    """
    convert the wcs headers to a json (and an intermediate text file)
    """
    R = {}
    text_file = WORKDIR.joinpath('calibration.txt')
    json_file = WORKDIR.joinpath('calibration.json')
    # convert to text file:
    with open(text_file,'w') as outfile:
        subprocess.Popen(['wcsinfo',wcs_file],cwd=WORKDIR.as_posix(),stdout=outfile).wait()
    # read the text file:
    with open(text_file,'r') as infile:
        rows = infile.read().split('\n')
    # convert to json:
    for r in rows:
        # try to cast to float, then int, else leave it a string:
        if r:
            s = r.split()
            k = s[0]
            v = s[1]
            try:
                V = float(v)
                if V.is_integer():
                    V = int(v)
            except:
                V = v
            R[k] = V
    with open(json_file,'w') as outfile:
        json.dump(R,outfile,indent=4)
    return R

def getObjectsInField(WORKDIR,wcs_file='field.wcs'):
    """
    generate a txt and json file of the objects in the field
    """
    text_file = WORKDIR.joinpath('objectsinfield.txt')
    json_file = WORKDIR.joinpath('objectsinfield.json')
    # convert to text file:
    with open(text_file,'w') as outfile:
        subprocess.Popen('plot-constellations -w {} -N -C -B -b 10 -j -L'.format(wcs_file).split(' '),cwd=WORKDIR.as_posix(),stdout=outfile).wait()
    # read the text file:
    with open(text_file,'r') as infile:
        stars = infile.read().strip().split('\n')
    # get rid of empty strings:
    stars = [ s for s in stars if len(s) > 0]
    # write the json file:
    with open(json_file,'w') as outfile:
        json.dump(stars,outfile,indent=4)
    return stars
    

if __name__ == "__main__":
    wcs2json(Path('.'))
